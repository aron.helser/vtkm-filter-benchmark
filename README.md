# vtkm-filter-benchmark

Compare vtk and vtkm filters using a python script and pipeline. 

Inital bench_memory.bash callsthreshold_both.py script and prints timings to the console, and generates a memory plot using mprof. Memory use was esitamted from the plot, and put in a [google spreadsheet](https://docs.google.com/spreadsheets/d/15T6GQA8QiavglOsBFHKWfuU8X-CO4Sl8g8_CTDjH5cQ/edit#gid=0). 

benchmark.py and run-filter.py is the next version, which is driven entirely in python, and reads the mprof output to get exact numbers for timing and memory use. It runs vtk, vtk-m plus tbb, and vtk-m plus cuda for each filter, and records time and memory use. Results are appended to the google spreadsheet.

The module [memory_profiler](https://pypi.org/project/memory_profiler/) is essential to tracking memory use and timing for the app. 

## Use

* Use python 3.6, probably. 
* `python3.6 -m pip install --user memory_profiler matplotlib`
* also possibly need python3-tk tkinter
* edit benchmark.py to point to vtk builds with vtk-m enabled. Two builds with thread-building-blocks (TBB) and Cuda are needed.
* edit run-filter.py to set the input size, if desired.
* `python3.6 benchmark.py |& tee all_240.txt`
* each benchmark produces a .dat and .png, with memory_profiler data and plot.
* log prints lines like this:
	* threshold_vtk 818.828125 8.620500087738037
	* filter_type, memory used in Mb, time in sec
* fairly easy to copy-paste into a google spreadsheet
* sort by filter and size, for easy graph ordering.
* need to move each filter's results to its own column, so sheets regards them as separate series when creating the graph
	* use row X as header
	* aggregate column Y
	* vertical axis label "time" or "memory"
	* vertical axis log scale
* Recorded a macro to produce a graph from a data range, moving the data to different columns then triggering the graph. Relative sheet postion.