import sys, os, os.path
from subprocess import call

def get_mem(fn):
  memFile = open(fn)
  maxMem = 0
  minMem = 0
  startTime = 0
  endTime = 0
  runTime = 0
  memList = []
  timeList = []
  for line in memFile.readlines():
    # The 'FUNC' line tells when the benchmark ran. Appears near the end
    if line.startswith("FUNC"):
      toks = line.split(" ")
      # read samples +-0.3 sec before start/end, during sleep
      startTime = float(toks[3]) - 0.3
      endTime = float(toks[5]) + 0.3
      runTime = float(toks[5]) - float(toks[3])
      minMem = min(float(toks[2]), float(toks[4]))
      maxMem = max(float(toks[2]), float(toks[4]))
    if not line.startswith("MEM"):
      continue
    toks = line.split(" ")
    memList.append(float(toks[1]))
    timeList.append(float(toks[2]))

  for i in range(len(memList)):
    if timeList[i] > startTime and timeList[i] < endTime:
      minMem = min(minMem, memList[i])
      maxMem = max(maxMem, memList[i])

  # print("memDiff", maxMem - minMem)
  # print("runTime", runTime)
  return (maxMem - minMem, runTime)

def runMprof():
  #benchmarks=["threshold", "gradient", "normals", "contour", "cell2point"] # "clean" - paraview only
  benchmarks = ["contour"]
  # CUSTOMIZE: base path for vtk builds:
  basepath ="/home/kitware/aron.helser/vtk/"
  tbbPath = basepath + "build_tbb/"
  cudaPath = basepath + "build_cuda/"
  # Need to fix environ so 'import vtk' works with plain python:
  savepath = os.environ["PATH"]
  savePythonpath = os.environ.get("PYTHONPATH", "")
  saveLdpath = os.environ.get("LD_LIBRARY_PATH", "")

  pythonVer = "2.7"

  configs = [
    ("vtk", tbbPath, "vtk"),
    ("vtkm_tbb", tbbPath, "vtkm"),
    ("vtkm_cuda", cudaPath, "vtkm")
  ]

  for bench in benchmarks:

    for config in configs:
      name, configPath, variant = config
      # setup for base vtk and vtkm TBB
      os.environ["PATH"] = configPath + "bin" + os.pathsep + savepath
      os.environ["LD_LIBRARY_PATH"] = configPath + "lib" + os.pathsep + saveLdpath
      os.environ["PYTHONPATH"] = configPath + "lib/python" + pythonVer + "/site-packages" + os.pathsep + savePythonpath
      basename = bench + "_" + name
      dat = basename + ".dat"
      # mprof will append to the log file if we don't remove it.
      if os.path.exists(dat):
        os.remove(dat)

      cmd = "mprof run --python -o " + dat + " run-filter.py " + bench + " " + variant
      call(cmd, shell=True)
      usedMem, runTime = get_mem(dat)
      # save to file?
      print("%s, %f, %f" % (basename, usedMem, runTime))
      # call("mprof plot -o " + basename + ".png " + dat, shell=True)
      # show the resulting png, only for single runs?
      # call("xdg-open " + basename + ".png ", shell=True)

  return 0

if __name__ == '__main__':
  if len(sys.argv) < 1:
    print('usage: <mprof file>')
    exit(0)
  else:
    exit(runMprof())
