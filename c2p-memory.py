import vtk

w = vtk.vtkRTAnalyticSource()
w.SetWholeExtent(0, 250, 0, 250, 0, 250)

t = vtk.vtkDataSetTriangleFilter()
t.SetInputConnection(w.GetOutputPort())

t.Update()

print("tets ready")

p2c = vtk.vtkmAverageToCells()
p2c.SetInputData(t.GetOutput())
p2c.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "RTData")
p2c.Update()

print("p2c done")

import time
time.sleep(5)

c2p = vtk.vtkmAverageToPoints()
c2p.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, "RTData")
c2p.SetInputData(p2c.GetOutput())
c2p.Update()

print("c2p done")

time.sleep(15)
