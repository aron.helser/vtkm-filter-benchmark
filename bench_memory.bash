#! /bin/bash
benchmarks=( "threshold" "gradient" "normals" "contour" "cell2point" ) # "clean" - paraview only

for item in "${benchmarks[@]}"
do
	mprof run ../vtk/build_py3/bin/vtkpython -m memory_profiler threshold-both.py $item
	mprof plot --output ${item}.png
done