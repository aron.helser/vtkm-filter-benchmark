import vtk
from timeit import default_timer as timer


w = vtk.vtkRTAnalyticSource()
w.SetWholeExtent(0, 250, 0, 250, 0, 250)

t = vtk.vtkDataSetTriangleFilter()
t.SetInputConnection(w.GetOutputPort())

t.Update()

print("tets ready")

start = timer()
p2c = vtk.vtkmAverageToCells()
p2c.SetInputData(t.GetOutput())
p2c.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "RTData")
p2c.Update()
end = timer()

print("p2c done", end - start)

import time
time.sleep(2)

start = timer()
c2p = vtk.vtkmAverageToPoints()
c2p.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, "RTData")
# c2p = vtk.vtkCellDataToPointData()
c2p.SetInputData(p2c.GetOutput())
c2p.Update()
end = timer()

print("c2p done", end - start)

# clean up
#c2p.SetInputData(None)
del c2p
time.sleep(2)

start = timer()
c2p = vtk.vtkCellDataToPointData()
c2p.SetInputData(p2c.GetOutput())
c2p.Update()
end = timer()

print("c2p vtk done", end - start)

time.sleep(0.5)
