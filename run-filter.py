import sys
import vtk
from timeit import default_timer as timer

def run(case, variant):
  print("start", case, variant)

  zero = timer()
  w = vtk.vtkRTAnalyticSource()
  num = 320 # 60 # 120 # 240 # 160, 240, 320
  w.SetWholeExtent(0, num, 0, num, 0, num)

  # tetrahedralize filter in ParaView
  t = vtk.vtkDataSetTriangleFilter()
  t.SetInputConnection(w.GetOutputPort())

  t.Update()
  print("tetras setup done")

  waveletRange = w.GetOutput().GetPointData().GetScalars().GetRange()
  # print(waveletRange)

  # general cell data:
  # start = timer()
  p2c = vtk.vtkmAverageToCells()


  if case == "contour":
    p2c.SetInputData(w.GetOutput())
  else:
    p2c.SetInputData(t.GetOutput())
  p2c.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "RTData")
  p2c.Update()
  # end = timer()
  # print("p2c done", end - start)

  # case = 4
  filter1def = None
  filter2def = None
  cells = False
  params = None
  output = None
  dataName = ""

  if case == "threshold":
    # Threshold
    cells = False
    filter1def = vtk.vtkmThreshold
    filter2def = vtk.vtkThreshold
    params = lambda x: x.ThresholdBetween(80, 200)
    output = t.GetOutput()
    dataName = "RTData"

  elif case == "gradient":
    # Gradient
    cells = False
    filter1def = vtk.vtkmGradient
    filter2def = vtk.vtkGradientFilter
    # divergence, vorticity, QCriteria need 3 component input. vtkBrownianPoints...
    def gradientParams(x):
      x.SetComputeDivergence(True)
      x.SetComputeVorticity(True)
    params = gradientParams
    randVec = vtk.vtkBrownianPoints()
    randVec.SetInputData(t.GetOutput())
    randVec.Update()
    output = randVec.GetOutput()
    dataName = "BrownianVectors"

  elif case == "normals":
    # Normals
    # setup - need a surface
    if 0:
      # tests take < 0.1 sec
      threshold = vtk.vtkmThreshold()
      threshold.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "RTData")
      threshold.SetInputData(w.GetOutput())
      threshold.ThresholdBetween(100, 200)
      threshold.Update()
      surf = vtk.vtkDataSetSurfaceFilter()
      surf.SetInputData(threshold.GetOutput())
      surf.Update()
      output = surf.GetOutput()
    else:
      # tests take > 1 sec
      cont = vtk.vtkmContour()
      cont.GenerateValues(40, waveletRange[0], waveletRange[1])
      cont.SetComputeScalars(1)
      cont.SetComputeNormals(0)
      cont.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "RTData")
      cont.SetInputData(w.GetOutput())
      cont.Update()
      output = cont.GetOutput()

    cells = False
    filter1def = vtk.vtkmPolyDataNormals
    filter2def = vtk.vtkPolyDataNormals # vtk.vtkPolyDataNormals
    params = lambda x: (x.SetSplitting(0) or x.SetConsistency(0))
    dataName = "RTData"

  elif case == "contour":
    # Contour
    cells = False
    filter1def = vtk.vtkmContour
    filter2def = vtk.vtkContourFilter # vtkPVContourFilter
    def setParams(x):
      x.GenerateValues(30, waveletRange[0], waveletRange[1])
      x.SetComputeScalars(1)
      x.SetComputeNormals(1)

    params = setParams
    output = p2c.GetOutput()
    dataName = "RTData"

  elif case == "cell2point":
    # c2p
    cells = True
    filter1def = vtk.vtkmAverageToPoints
    filter2def = vtk.vtkCellDataToPointData
    params = lambda x: None
    output = p2c.GetOutput()
    dataName = "RTData"

  elif case == "clean":
    # clean
    cells = False
    filter1def = vtk.vtkmCleanGrid
    # Nope, this is a paraview extension:
    filter2def = vtk.vtkCleanUnstructuredGrid
    # what about vtkCleanPolyData ? Not the same.

    params = lambda x: None # x.SetCompactPoints(1) if type(x) is vtk.vtkmCleanGrid else None
    output = t.GetOutput()
    dataName = "RTData"

  print("setup done")


  import time
  time.sleep(1)

  filter1 = None
  @profile
  def vtkmBench():
    start = timer()
    filter1 = filter1def()
    if cells:
      filter1.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, dataName)
    else:
      filter1.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, dataName)
    filter1.SetInputData(output)
    params(filter1)
    filter1.Update()
    end = timer()

    print(case, "vtkm: ", end - start) #, "elapsed", end - zero)

  if variant == "vtkm":
    vtkmBench()

    # clean up
    #filter1.SetInputData(None)
    del filter1

    time.sleep(1)

  filter2 = None
  @profile
  def vtkBench():
    start = timer()
    filter2 = filter2def()
    if cells:
      filter2.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, dataName)
    else:
      filter2.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, dataName)
    filter2.SetInputData(output)
    params(filter2)
    filter2.Update()
    end = timer()

    print(case, "vtk: ", end - start) #, "elapsed", end - zero)

  if variant == "vtk":
    vtkBench()

    del filter2

    time.sleep(1)

  return 0

if __name__ == '__main__':
  if len(sys.argv) < 2:
    print('usage: <bench #>')
    exit(0)
  else:
    arg2 = sys.argv[2] if len(sys.argv) > 2 else "vtk"
    exit(run(sys.argv[1], arg2))
